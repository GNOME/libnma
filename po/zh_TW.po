# Translation of libnma to Traditional Chinese.
# Traditional Chinese translation of libnma.
# Copyright (C) 2005, 2010, 2013 Free Software Foundation, Inc.
# Woodman Tuen <wmtuen@gmail.com>, 2005.
# Hialan Liu <hialan.liu@gmail.com>, 2008.
# Chester Cheng <ccheng@redhat.com>, 2010.
# Terry Chuang <tchuang@redhat.com>, 2010.
# Wei-Lun Chao <chaoweilun@gmail.com>, 2010.
# Roy Chan <roy.chan@linux.org.hk>, 2011.
# Anthony Fok <anthony.t.fok@gmail.com>, 2013.
# lrintel <lrintel@redhat.com>, 2017. #zanata
msgid ""
msgstr ""
"Project-Id-Version: libnma\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/libnma/\n"
"POT-Creation-Date: 2022-11-22 13:08+0100\n"
"PO-Revision-Date: 2017-11-07 06:45+0000\n"
"Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>\n"
"Language-Team: Traditional Chinese <zh-l10n@lists.linux.org.tw>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Zanata 4.3.2\n"

#: org.gnome.nm-applet.eap.gschema.xml.in:6
#: org.gnome.nm-applet.eap.gschema.xml.in:11
msgid "Ignore CA certificate"
msgstr "忽略 CA 憑證"

#: org.gnome.nm-applet.eap.gschema.xml.in:7
msgid ""
"Set this to true to disable warnings about CA certificates in EAP "
"authentication."
msgstr "將這設為 True，以停用在 EAP 認證中關於 CA 證書的警告。"

#: org.gnome.nm-applet.eap.gschema.xml.in:12
msgid ""
"Set this to true to disable warnings about CA certificates in phase 2 of EAP "
"authentication."
msgstr "將這設為 True，以停用在 EAP 認證第二階段中關於 CA 證書的警告。"

#: shared/nm-utils/nm-shared-utils.c:793
#, c-format
msgid "object class '%s' has no property named '%s'"
msgstr "對像類'%s'沒有名字'%s“"

#: shared/nm-utils/nm-shared-utils.c:800
#, c-format
msgid "property '%s' of object class '%s' is not writable"
msgstr "物業'%s'對像類'%s'不可寫"

#: shared/nm-utils/nm-shared-utils.c:807
#, c-format
msgid ""
"construct property \"%s\" for object '%s' can't be set after construction"
msgstr "建築物“%s“對象”%s'施工後無法設置"

#: shared/nm-utils/nm-shared-utils.c:815
#, c-format
msgid "'%s::%s' is not a valid property name; '%s' is not a GObject subtype"
msgstr "“%s::%s'不是有效的財產名稱; “%s'不是GObject子類型"

#: shared/nm-utils/nm-shared-utils.c:824
#, c-format
msgid "unable to set property '%s' of type '%s' from value of type '%s'"
msgstr "無法設置屬性'%s'類型'%s'來自類型的價值'%s“"

#: shared/nm-utils/nm-shared-utils.c:835
#, c-format
msgid ""
"value \"%s\" of type '%s' is invalid or out of range for property '%s' of "
"type '%s'"
msgstr "價值“%s“類型'%s'無效或超出財產範圍'%s'類型'%s“"

#: src/nma-bar-code-widget.c:142
msgid "Network"
msgstr "網路"

#: src/nma-bar-code-widget.c:159
#, fuzzy
msgid "Password"
msgstr "密碼："

#: src/nma-bar-code-widget.ui:35
msgid "Scan with your phone or <a href=\"nma:print\">Print</a>"
msgstr ""

#: src/nma-cert-chooser.c:513
#, fuzzy
msgid "No certificate set"
msgstr "不需要 CA 憑證(_R)"

#: src/nma-cert-chooser.c:537
msgid "No key set"
msgstr "沒有按鍵設置"

#: src/nma-cert-chooser.c:860
#, fuzzy, c-format
msgid "Choose a %s Certificate"
msgstr "選擇 CA 憑證"

#: src/nma-cert-chooser.c:864
#, fuzzy, c-format
msgid "%s _certificate"
msgstr "使用者憑證(_U)："

#: src/nma-cert-chooser.c:868
#, fuzzy, c-format
msgid "%s certificate _password"
msgstr "使用者憑證(_U)："

#: src/nma-cert-chooser.c:887
#, fuzzy, c-format
msgid "Choose a key for %s Certificate"
msgstr "選擇 CA 憑證"

#: src/nma-cert-chooser.c:891
#, fuzzy, c-format
msgid "%s private _key"
msgstr "私密金鑰(_K)："

#: src/nma-cert-chooser.c:895
#, fuzzy, c-format
msgid "%s key _password"
msgstr "私密金鑰密碼(_P)："

#: src/nma-cert-chooser.c:1125
msgid "Sho_w passwords"
msgstr "顯示密碼(_W)"

#: src/nma-cert-chooser-button.c:177
#, c-format
msgid "Key in %s"
msgstr "鍵入 %s"

#: src/nma-cert-chooser-button.c:178
#, fuzzy, c-format
msgid "Certificate in %s"
msgstr "C_A 憑證："

#: src/nma-cert-chooser-button.c:209 src/nma-cert-chooser-button.c:320
msgid "Select"
msgstr "選擇"

#: src/nma-cert-chooser-button.c:210 src/nma-cert-chooser-button.c:321
msgid "Cancel"
msgstr "取消"

#: src/nma-cert-chooser-button.c:277 src/nma-ws/nma-eap-fast.c:330
msgid "(None)"
msgstr "（無）"

#: src/nma-cert-chooser-button.c:300 src/nma-pkcs11-cert-chooser-dialog.c:189
msgid "(Unknown)"
msgstr "（不明）"

#: src/nma-cert-chooser-button.c:431
msgid "Select from file…"
msgstr "從文件中選擇..."

#: src/nma-mobile-providers.c:787
msgid "Default"
msgstr "預設"

#: src/nma-mobile-providers.c:976
msgid "My country is not listed"
msgstr "我的國家並沒有列出來"

#: src/nma-mobile-wizard.c:142
msgid "GSM"
msgstr "GSM"

#: src/nma-mobile-wizard.c:145
msgid "CDMA"
msgstr "CDMA"

#: src/nma-mobile-wizard.c:250 src/nma-mobile-wizard.c:282
msgid "Unlisted"
msgstr "未列出的"

#: src/nma-mobile-wizard.c:481
#, fuzzy
msgid "My plan is not listed…"
msgstr "我的方案沒有列出…"

#: src/nma-mobile-wizard.c:652
msgid "Provider"
msgstr "供應商"

#: src/nma-mobile-wizard.c:1024
msgid "Installed GSM device"
msgstr "已安裝的 GSM 裝置"

#: src/nma-mobile-wizard.c:1027
msgid "Installed CDMA device"
msgstr "已安裝的 CDMA 裝置"

#: src/nma-mobile-wizard.c:1232
msgid "Any device"
msgstr "任何裝置"

#: src/nma-mobile-wizard.ui:49
msgid "New Mobile Broadband Connection"
msgstr "新的行動寬頻連線"

#: src/nma-mobile-wizard.ui:66
msgid ""
"This assistant helps you easily set up a mobile broadband connection to a "
"cellular (3G) network."
msgstr "這能幫助您輕易地設定行動寬頻連線，連至手機 (3G) 網路。"

#: src/nma-mobile-wizard.ui:81
msgid "You will need the following information:"
msgstr "您需要以下資訊："

#: src/nma-mobile-wizard.ui:96
#, fuzzy
msgid "Your broadband provider’s name"
msgstr "寬頻供應商的名稱"

#: src/nma-mobile-wizard.ui:110
msgid "Your broadband billing plan name"
msgstr "寬頻計費方案的名稱"

#: src/nma-mobile-wizard.ui:124
msgid "(in some cases) Your broadband billing plan APN (Access Point Name)"
msgstr "（在有些情況下）您的寬頻計費方案的 APN（存取點名稱）"

#: src/nma-mobile-wizard.ui:138
msgid "Create a connection for _this mobile broadband device:"
msgstr "為這個行動寬頻裝置建立連線(_T)"

#: src/nma-mobile-wizard.ui:164
msgid "Set up a Mobile Broadband Connection"
msgstr "設定行動寬頻連線"

#: src/nma-mobile-wizard.ui:182
#, fuzzy
msgid "Country or region:"
msgstr "國家或地區"

#: src/nma-mobile-wizard.ui:220
#, fuzzy
msgid "Choose your Provider’s Country or Region"
msgstr "選擇您的供應商的國家或地區"

#: src/nma-mobile-wizard.ui:235
msgid "Select your provider from a _list:"
msgstr "請從清單裡選擇供應商(_L)："

#: src/nma-mobile-wizard.ui:277
#, fuzzy
msgid "I can’t find my provider and I wish to set up the connection _manually:"
msgstr "我找不到我的供應商，我想要手動輸入(_M)："

#: src/nma-mobile-wizard.ui:298
msgid "My provider uses GSM technology (GPRS, EDGE, UMTS, HSPA)"
msgstr "我的供應商使用 GSM 技術（GPRS、EDGE、UMTS、HSPA）"

#: src/nma-mobile-wizard.ui:299
msgid "My provider uses CDMA technology (1xRTT, EVDO)"
msgstr "我的供應商使用 CDMA 技術（1xRTT、EVDO）"

#: src/nma-mobile-wizard.ui:310
msgid "Choose your Provider"
msgstr "選擇您的供應商"

#: src/nma-mobile-wizard.ui:327
msgid "_Select your plan:"
msgstr "選擇您的方案(_S)："

#: src/nma-mobile-wizard.ui:353
msgid "Selected plan _APN (Access Point Name):"
msgstr "選擇方案的 _APN（Access Point Name，存取點名稱）："

#: src/nma-mobile-wizard.ui:401
#, fuzzy
msgid ""
"Warning: Selecting an incorrect plan may result in billing issues for your "
"broadband account or may prevent connectivity.\n"
"\n"
"If you are unsure of your plan please ask your provider for your plan’s APN."
msgstr ""
"警告：選擇不正確的方案可能會引起寬頻帳號的收費爭議，或讓您無法連線。\n"
"\n"
"如果您不確定所使用的方案，請洽詢供應商，以得知方案的 APN 為何。"

#: src/nma-mobile-wizard.ui:422
msgid "Choose your Billing Plan"
msgstr "選擇您的計費方案"

#: src/nma-mobile-wizard.ui:440
msgid ""
"Your mobile broadband connection is configured with the following settings:"
msgstr "行動寬連線已經配置了以下設定："

#: src/nma-mobile-wizard.ui:454
msgid "Your Device:"
msgstr "您的裝置："

#: src/nma-mobile-wizard.ui:480
msgid "Your Provider:"
msgstr "您的供應商："

#: src/nma-mobile-wizard.ui:506
msgid "Your Plan:"
msgstr "您的方案："

#: src/nma-mobile-wizard.ui:561
#, fuzzy
msgid ""
"A connection will now be made to your mobile broadband provider using the "
"settings you selected. If the connection fails or you cannot access network "
"resources, double-check your settings. To modify your mobile broadband "
"connection settings, choose “Network Connections” from the System → "
"Preferences menu."
msgstr ""
"現在，根據您所選取的設定，電腦將連上您的行動寬頻供應商。如果連線失敗，或是您"
"無法存取網路資源，請檢查設定。要修改行動寬頻連線的設定，請從「系統」 >> 「偏"
"好設定」中，選擇「網路連線」。"

#: src/nma-mobile-wizard.ui:575
msgid "Confirm Mobile Broadband Settings"
msgstr "確認行動寬頻設定"

#: src/nma-pkcs11-cert-chooser-dialog.c:260
msgid "Error logging in: "
msgstr "登錄時出錯： "

#: src/nma-pkcs11-cert-chooser-dialog.c:282
#, fuzzy
msgid "Error opening a session: "
msgstr "編輯連線時發生錯誤"

#: src/nma-pkcs11-cert-chooser-dialog.ui:18
#, fuzzy
msgid "_Unlock token"
msgstr "解鎖(_U)"

#: src/nma-pkcs11-cert-chooser-dialog.ui:99
msgid "Name"
msgstr "名稱"

#: src/nma-pkcs11-cert-chooser-dialog.ui:109
msgid "Issued By"
msgstr "由...發出"

#: src/nma-pkcs11-token-login-dialog.c:134
#, c-format
msgid "Enter %s PIN"
msgstr "輸入 %s 銷"

#: src/nma-pkcs11-token-login-dialog.ui:19 src/nma-vpn-password-dialog.ui:28
#: src/nma-wifi-dialog.c:1127 src/nma-ws/nma-eap-fast.ui:27
msgid "_Cancel"
msgstr "取消 (_C)"

#: src/nma-pkcs11-token-login-dialog.ui:34
msgid "_Login"
msgstr "登入(_L)"

#: src/nma-pkcs11-token-login-dialog.ui:81
msgid "_Remember PIN"
msgstr "_記住密碼"

#: src/nma-ui-utils.c:34
#, fuzzy
msgid "Store the password only for this user"
msgstr "只儲存這個使用者的密碼(_U)"

#: src/nma-ui-utils.c:35
#, fuzzy
msgid "Store the password for all users"
msgstr "儲存所有使用者的密碼(_A)"

#: src/nma-ui-utils.c:36
msgid "Ask for this password every time"
msgstr "每次都詢問此密碼"

#: src/nma-ui-utils.c:37
#, fuzzy
msgid "The password is not required"
msgstr "連線至「%s」需要密碼。"

#: src/nma-vpn-password-dialog.ui:43
msgid "_OK"
msgstr "確定(_O)"

#: src/nma-vpn-password-dialog.ui:76
msgid "Sh_ow passwords"
msgstr "顯示密碼(_O)"

#: src/nma-vpn-password-dialog.ui:133
#, fuzzy
msgid "_Tertiary Password:"
msgstr "第二密碼(_S)："

#: src/nma-vpn-password-dialog.ui:147
msgid "_Secondary Password:"
msgstr "第二密碼(_S)："

#: src/nma-vpn-password-dialog.ui:161
msgid "_Password:"
msgstr "密碼(_P)："

#: src/nma-wifi-dialog.c:116
#, fuzzy
msgid "Click to connect"
msgstr "斷線"

#: src/nma-wifi-dialog.c:443
msgid "New…"
msgstr "新…"

#: src/nma-wifi-dialog.c:939
msgctxt "Wifi/wired security"
msgid "None"
msgstr "沒有"

#: src/nma-wifi-dialog.c:955
msgid "WEP 40/128-bit Key (Hex or ASCII)"
msgstr "WEP 40/128-位元 金鑰 (Hex 或 ASCII)"

#: src/nma-wifi-dialog.c:962
msgid "WEP 128-bit Passphrase"
msgstr "WEP 128-位元 密語"

#: src/nma-wifi-dialog.c:977 src/nma-ws/nma-ws-802-1x.c:367
msgid "LEAP"
msgstr "LEAP"

#: src/nma-wifi-dialog.c:988
msgid "Dynamic WEP (802.1x)"
msgstr "動態 WEP（802.1x）"

#: src/nma-wifi-dialog.c:1000
msgid "WPA & WPA2 Personal"
msgstr "WPA & WPA2 個人版"

#: src/nma-wifi-dialog.c:1016
msgid "WPA & WPA2 Enterprise"
msgstr "WPA & WPA2 企業版"

#: src/nma-wifi-dialog.c:1027
#, fuzzy
msgid "WPA3 Personal"
msgstr "WPA & WPA2 個人版"

#: src/nma-wifi-dialog.c:1038
msgid "Enhanced Open"
msgstr ""

#: src/nma-wifi-dialog.c:1131
msgid "C_reate"
msgstr "建立(_R)"

#: src/nma-wifi-dialog.c:1133
msgid "C_onnect"
msgstr "連線(_O)"

#: src/nma-wifi-dialog.c:1211
#, fuzzy, c-format
msgid ""
"Passwords or encryption keys are required to access the Wi-Fi network “%s”."
msgstr "需要密碼或是加密金鑰來存取 Wi-Fi 網路「%s」。"

#: src/nma-wifi-dialog.c:1213
msgid "Wi-Fi Network Authentication Required"
msgstr "Wi-Fi 網路連線需要驗證"

#: src/nma-wifi-dialog.c:1215
msgid "Authentication required by Wi-Fi network"
msgstr "Wi-Fi 網路連線需要驗證"

#: src/nma-wifi-dialog.c:1220
msgid "Create New Wi-Fi Network"
msgstr "建立新的 Wi-Fi 網路"

#: src/nma-wifi-dialog.c:1222
msgid "New Wi-Fi network"
msgstr "新的 Wi-Fi 網路"

#: src/nma-wifi-dialog.c:1223
msgid "Enter a name for the Wi-Fi network you wish to create."
msgstr "輸入您要建立的 Wi-Fi 網路的名稱。"

#: src/nma-wifi-dialog.c:1225
msgid "Connect to Hidden Wi-Fi Network"
msgstr "連接到隱藏的 Wi-Fi 網路"

#: src/nma-wifi-dialog.c:1227
msgid "Hidden Wi-Fi network"
msgstr "隱藏的 Wi-Fi 網路"

#: src/nma-wifi-dialog.c:1228
msgid ""
"Enter the name and security details of the hidden Wi-Fi network you wish to "
"connect to."
msgstr "輸入您要連接的隱藏 Wi-Fi 網路的名稱與安全性詳細資料。"

#: src/nma-ws/nma-eap-fast.c:60
msgid "missing EAP-FAST PAC file"
msgstr "缺少EAP-FAST PAC文件"

#: src/nma-ws/nma-eap-fast.c:250 src/nma-ws/nma-eap-peap.c:310
#: src/nma-ws/nma-eap-ttls.c:363
msgid "GTC"
msgstr "GTC"

#: src/nma-ws/nma-eap-fast.c:266 src/nma-ws/nma-eap-peap.c:278
#: src/nma-ws/nma-eap-ttls.c:297
msgid "MSCHAPv2"
msgstr "MSCHAPv2"

#: src/nma-ws/nma-eap-fast.c:449
msgid "PAC files (*.pac)"
msgstr "PAC 檔案 (*.pac)"

#: src/nma-ws/nma-eap-fast.c:453
msgid "All files"
msgstr "所有檔案"

#: src/nma-ws/nma-eap-fast.ui:19
#, fuzzy
msgid "Choose a PAC file"
msgstr "選擇一個 PAC 檔案…"

#: src/nma-ws/nma-eap-fast.ui:36
msgid "_Open"
msgstr "開啟(_O)"

#: src/nma-ws/nma-eap-fast.ui:72
msgid "Anonymous"
msgstr "匿名"

#: src/nma-ws/nma-eap-fast.ui:75
msgid "Authenticated"
msgstr "已驗證"

#: src/nma-ws/nma-eap-fast.ui:78
msgid "Both"
msgstr "兩者"

#: src/nma-ws/nma-eap-fast.ui:91 src/nma-ws/nma-eap-peap.ui:42
#: src/nma-ws/nma-eap-ttls.ui:113
#, fuzzy
msgid "Anony_mous identity"
msgstr "匿名識別(_M)："

#: src/nma-ws/nma-eap-fast.ui:117
#, fuzzy
msgid "PAC _file"
msgstr "PAC 檔案(_F)："

#: src/nma-ws/nma-eap-fast.ui:188 src/nma-ws/nma-eap-peap.ui:115
#: src/nma-ws/nma-eap-ttls.ui:71
#, fuzzy
msgid "_Inner authentication"
msgstr "內部驗證(_I)："

#: src/nma-ws/nma-eap-fast.ui:217
msgid "Allow automatic PAC pro_visioning"
msgstr "允許自動供應 PAC (_V)"

#: src/nma-ws/nma-eap-leap.c:55
msgid "missing EAP-LEAP username"
msgstr "缺少EAP-LEAP用戶名"

#: src/nma-ws/nma-eap-leap.c:64
msgid "missing EAP-LEAP password"
msgstr "缺少EAP-LEAP密碼"

#: src/nma-ws/nma-eap-leap.ui:15 src/nma-ws/nma-eap-simple.ui:15
#: src/nma-ws/nma-ws-leap.ui:15
#, fuzzy
msgid "_Username"
msgstr "使用者名稱(_U)："

#: src/nma-ws/nma-eap-leap.ui:29 src/nma-ws/nma-eap-simple.ui:29
#: src/nma-ws/nma-ws-leap.ui:29 src/nma-ws/nma-ws-sae.ui:14
#: src/nma-ws/nma-ws-wpa-psk.ui:14
#, fuzzy
msgid "_Password"
msgstr "密碼(_P)："

#: src/nma-ws/nma-eap-leap.ui:54 src/nma-ws/nma-eap-simple.ui:71
#: src/nma-ws/nma-ws-leap.ui:55 src/nma-ws/nma-ws-sae.ui:56
#: src/nma-ws/nma-ws-wpa-psk.ui:55
msgid "Sho_w password"
msgstr "顯示密碼(_W)"

#: src/nma-ws/nma-eap-peap.c:294 src/nma-ws/nma-eap-ttls.c:347
#: src/nma-ws/nma-ws-802-1x.c:343
msgid "MD5"
msgstr "MD5"

#: src/nma-ws/nma-eap-peap.ui:23
msgid "Automatic"
msgstr "自動"

#: src/nma-ws/nma-eap-peap.ui:26
msgid "Version 0"
msgstr "版本 0"

#: src/nma-ws/nma-eap-peap.ui:29
msgid "Version 1"
msgstr "版本 1"

#: src/nma-ws/nma-eap-peap.ui:66 src/nma-ws/nma-eap-tls.ui:38
#: src/nma-ws/nma-eap-ttls.ui:83
msgid "No CA certificate is _required"
msgstr "不需要 CA 憑證(_R)"

#: src/nma-ws/nma-eap-peap.ui:83
#, fuzzy
msgid "PEAP _version"
msgstr "PEAP 版本(_V)："

#: src/nma-ws/nma-eap-peap.ui:162 src/nma-ws/nma-eap-tls.ui:56
#: src/nma-ws/nma-eap-ttls.ui:127
msgid "Suffix of the server certificate name."
msgstr "服務器證書名稱的後綴。"

#: src/nma-ws/nma-eap-peap.ui:163 src/nma-ws/nma-eap-tls.ui:57
#: src/nma-ws/nma-eap-ttls.ui:128
msgid "_Domain"
msgstr ""

#: src/nma-ws/nma-eap-simple.c:79
msgid "missing EAP username"
msgstr "缺少EAP用戶名"

#: src/nma-ws/nma-eap-simple.c:95
msgid "missing EAP password"
msgstr "缺少EAP密碼"

#: src/nma-ws/nma-eap-simple.c:109
msgid "missing EAP client Private Key passphrase"
msgstr ""

#: src/nma-ws/nma-eap-simple.ui:97
#, fuzzy
msgid "P_rivate Key Passphrase"
msgstr "WEP 128-位元 密語"

#: src/nma-ws/nma-eap-simple.ui:122
#, fuzzy
msgid "Sh_ow passphrase"
msgstr "顯示密碼(_O)"

#: src/nma-ws/nma-eap-tls.c:47
msgid "missing EAP-TLS identity"
msgstr "缺少EAP-TLS身份"

#: src/nma-ws/nma-eap-tls.c:237
#, fuzzy
msgid "no user certificate selected"
msgstr "不需要 CA 憑證(_R)"

#: src/nma-ws/nma-eap-tls.c:242
msgid "selected user certificate file does not exist"
msgstr "選定的用戶證書文件不存在"

#: src/nma-ws/nma-eap-tls.c:262
msgid "no key selected"
msgstr "沒有選擇密鑰"

#: src/nma-ws/nma-eap-tls.c:267
msgid "selected key file does not exist"
msgstr "選定的密鑰文件不存在"

#: src/nma-ws/nma-eap-tls.ui:14
#, fuzzy
msgid "I_dentity"
msgstr "識別(_D)："

#: src/nma-ws/nma-eap-ttls.c:265
msgid "PAP"
msgstr "PAP"

#: src/nma-ws/nma-eap-ttls.c:281
msgid "MSCHAP"
msgstr "MSCHAP"

#: src/nma-ws/nma-eap-ttls.c:314
#, fuzzy
msgid "MSCHAPv2 (no EAP)"
msgstr "MSCHAPv2"

#: src/nma-ws/nma-eap-ttls.c:331
msgid "CHAP"
msgstr "CHAP"

#: src/nma-ws/nma-eap.c:40
msgid "undefined error in 802.1X security (wpa-eap)"
msgstr "802.1X安全性中的未定義錯誤（wpa-eap）"

#: src/nma-ws/nma-eap.c:348
#, fuzzy
msgid "no CA certificate selected"
msgstr "不需要 CA 憑證(_R)"

#: src/nma-ws/nma-eap.c:353
msgid "selected CA certificate file does not exist"
msgstr "選定的CA證書文件不存在"

#: src/nma-ws/nma-ws-802-1x.c:355
msgid "TLS"
msgstr "TLS"

#: src/nma-ws/nma-ws-802-1x.c:379
msgid "PWD"
msgstr "PWD"

#: src/nma-ws/nma-ws-802-1x.c:390
msgid "FAST"
msgstr "快"

#: src/nma-ws/nma-ws-802-1x.c:401
msgid "Tunneled TLS"
msgstr "穿隧式 TLS"

#: src/nma-ws/nma-ws-802-1x.c:412
msgid "Protected EAP (PEAP)"
msgstr "保護式 EAP (PEAP)"

#: src/nma-ws/nma-ws-802-1x.c:427
msgid "Unknown"
msgstr "不明"

#: src/nma-ws/nma-ws-802-1x.c:441
msgid "Externally configured"
msgstr ""

#: src/nma-ws/nma-ws-802-1x.ui:25 src/nma-ws/nma-ws-wep-key.ui:95
#, fuzzy
msgid "Au_thentication"
msgstr "驗證(_E)："

#: src/nma-ws/nma-ws-leap.c:71
msgid "missing leap-username"
msgstr "缺少leap-username"

#: src/nma-ws/nma-ws-leap.c:87
msgid "missing leap-password"
msgstr "缺少飛躍密碼"

#: src/nma-ws/nma-ws-sae.c:73
#, fuzzy
msgid "missing password"
msgstr "缺少EAP密碼"

#: src/nma-ws/nma-ws-sae.ui:44 src/nma-ws/nma-ws-wpa-psk.ui:43
#, fuzzy
msgid "_Type"
msgstr "類型(_T)："

#: src/nma-ws/nma-ws-wep-key.c:110
msgid "missing wep-key"
msgstr "缺少wep-key"

#: src/nma-ws/nma-ws-wep-key.c:117
#, c-format
msgid "invalid wep-key: key with a length of %zu must contain only hex-digits"
msgstr "無效的wep-key：密鑰長度為 %zu 必須只包含十六進制數字"

#: src/nma-ws/nma-ws-wep-key.c:125
#, c-format
msgid ""
"invalid wep-key: key with a length of %zu must contain only ascii characters"
msgstr "無效的wep-key：密鑰長度為 %zu 必須只包含ascii字符"

#: src/nma-ws/nma-ws-wep-key.c:131
#, c-format
msgid ""
"invalid wep-key: wrong key length %zu. A key must be either of length 5/13 "
"(ascii) or 10/26 (hex)"
msgstr ""
"無效的wep-key：錯誤的密鑰長度 %zu。密鑰長度必須為5/13（ascii）或10/26（hex）"

#: src/nma-ws/nma-ws-wep-key.c:138
msgid "invalid wep-key: passphrase must be non-empty"
msgstr "無效的wep-key：passphrase必須是非空的"

#: src/nma-ws/nma-ws-wep-key.c:140
msgid "invalid wep-key: passphrase must be shorter than 64 characters"
msgstr "無效的wep-key：passphrase必須短於64個字符"

#: src/nma-ws/nma-ws-wep-key.ui:12
msgid "Open System"
msgstr "開放系統"

#: src/nma-ws/nma-ws-wep-key.ui:15
msgid "Shared Key"
msgstr "共享金鑰"

#: src/nma-ws/nma-ws-wep-key.ui:26
msgid "1 (Default)"
msgstr "1 (預設值)"

#: src/nma-ws/nma-ws-wep-key.ui:48
#, fuzzy
msgid "_Key"
msgstr "金鑰(_K)："

#: src/nma-ws/nma-ws-wep-key.ui:77
msgid "Sho_w key"
msgstr "顯示金鑰(_W)"

#: src/nma-ws/nma-ws-wep-key.ui:128
#, fuzzy
msgid "WEP inde_x"
msgstr "WEP 索引(_X)："

#: src/nma-ws/nma-ws-wpa-psk.c:80
#, c-format
msgid ""
"invalid wpa-psk: invalid key-length %zu. Must be [8,63] bytes or 64 hex "
"digits"
msgstr ""
"無效的wpa-psk：無效的密鑰長度 %zu。必須是[8,63]個字節或64個十六進制數字"

#: src/nma-ws/nma-ws-wpa-psk.c:87
msgid "invalid wpa-psk: cannot interpret key with 64 bytes as hex"
msgstr "無效的wpa-psk：無法將64字節的密鑰解釋為十六進制"

#: src/nma-ws/nma-ws.c:42
msgid "Unknown error validating 802.1X security"
msgstr "驗證802.1X安全性的未知錯誤"

#. The %s is a mobile provider name, eg "T-Mobile"
#: src/utils/utils.c:161
#, c-format
msgid "%s connection"
msgstr "%s 連線"

#: src/utils/utils.c:462
#, fuzzy
msgid "PEM certificates (*.pem, *.crt, *.cer)"
msgstr "DER 或 PEM 憑證 (*.der, *.pem, *.crt, *.cer)"

#: src/utils/utils.c:475
msgid "DER, PEM, or PKCS#12 private keys (*.der, *.pem, *.p12, *.key)"
msgstr "DER、PEM 或是 PKCS#12 私人金鑰 (*.der, *.pem, *.p12, *.key)"

#: src/wifi.ui:97
#, fuzzy
msgid "Wi-Fi _security"
msgstr "Wi-Fi 安全性(_S)："

#: src/wifi.ui:129
#, fuzzy
msgid "_Network name"
msgstr "網路名稱(_N)："

#: src/wifi.ui:154
#, fuzzy
msgid "C_onnection"
msgstr "連線(_N)："

#: src/wifi.ui:179
#, fuzzy
msgid "Wi-Fi _adapter"
msgstr "Wi-Fi 介面卡(_A)："
